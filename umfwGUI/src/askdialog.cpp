#include "uproto.h"

#include "askdialog.h"

#include <QDataStream>
#include <QDir>
#include <QFile>


static QString settingsFilePath (void) {
  QString hsp = QDir::homePath();
  if (!hsp.isEmpty() && (hsp[hsp.length()-1] != '/' && hsp[hsp.length()-1] != '\\')) hsp += '/';
  hsp += ".umfw/";
  QDir d;
  d.mkdir(hsp);
  return hsp+"winpos.rc";
}


AskDialog::AskDialog (QWidget *parent) :
  QDialog(parent), mGotHostInfo(false), mDescr(""), mPid(0), mAppName(""), mIP(""), mPort(0), mHostName(""),
  mProto(0), mAction(0), mLookupId(-1)
{
  setupUi(this);
  setAttribute(Qt::WA_QuitOnClose, false);
  setAttribute(Qt::WA_DeleteOnClose, false);
  setWindowFlags(Qt::WindowStaysOnTopHint);

  QFile file(settingsFilePath());
  if (file.open(QIODevice::ReadOnly)) {
    QDataStream stream(&file);
    int x; stream >> x;
    int y; stream >> y;
    int w; stream >> w;
    int h; stream >> h;
    file.close();
    setGeometry(x, y, w, h);
  }
}


AskDialog::~AskDialog () {
  if (mPid > 0 && !mGotHostInfo) QHostInfo::abortHostLookup(mLookupId);

  QRect geo(geometry());
  QFile file(settingsFilePath());
  if (file.open(QIODevice::WriteOnly)) {
    QDataStream stream(&file);
    stream << geo.x();
    stream << geo.y();
    stream << geo.width();
    stream << geo.height();
    file.close();
  }
}


void AskDialog::updateInfo (void) {
  QString hn;
  if (mGotHostInfo) hn = QString(" [%1:%2]").arg(mHostName).arg(mPort);

  edDescr->setText(mDescr);
  lbAppName->setText(QString("[%1] %2").arg(mPid).arg(mAppName));
  lbIP->setText(mIP);
  lbPort->setText(QString("%1").arg(mPort));
  textInfo->setPlainText(QString("application '%1' wants to do '%2' to '%3:%4%5'").
    arg(mAppName).arg(mProto==IPCQ_PROTO_UDP?"UDP":"TCP").arg(mIP).arg(mPort).arg(hn));
}


void AskDialog::setInfo (quint8 proto, quint8 action, quint32 pid, const QString &ip, quint16 port,
  const QString &appName, const QString &descr)
{
  if (mPid > 0 && !mGotHostInfo) QHostInfo::abortHostLookup(mLookupId);
  mPid = pid;
  mDescr = descr;
  mIP = ip;
  mPort = port;
  mAppName = appName;
  mGotHostInfo = false;
  mHostName = QString("");
  mProto = proto;
  mAction = action;
  updateInfo();
  mLookupId = QHostInfo::lookupHost(ip, this, SLOT(onHostInfo(QHostInfo)));
}


void AskDialog::onHostInfo (const QHostInfo &host) {
  mGotHostInfo = true;
  if (host.error() != QHostInfo::NoError) {
    qDebug() << "Lookup failed:" << host.errorString();
    return;
  }
  mHostName = host.hostName();
  //qDebug() << "name:" << mHostName;

  updateInfo();
}
