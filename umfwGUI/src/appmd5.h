#ifndef APPMD5_H
#define APPMD5_H

#include "defaults.h"

#include <QByteArray>
#include <QCryptographicHash>
#include <QString>


bool md5HashFile (const QString &fname, QByteArray *newHash);

/*
 * 0: all ok
 * 1: new hash calculated
 * 2: can't calculate new hash
 */
int md5CheckFile (const QString &fname, const QByteArray &hash, QByteArray *newHash);


#endif
