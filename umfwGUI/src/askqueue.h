#ifndef ASKQUEUE_H
#define ASKQUEUE_H

#include <sys/types.h>
#include <unistd.h>

#include "defaults.h"
#include "uproto.h"

#include <QList>
#include <QLocalSocket>


class AskQueueItem : public QObject {
  Q_OBJECT

public:
  AskQueueItem (QObject *parent = 0) : QObject(parent) {}
  ~AskQueueItem () { if (mSk) delete mSk; }

  QLocalSocket *mSk;
  pid_t mPid;
  QString mAppName;
  QString mIPStr;
  quint32 mIP;
  quint16 mPort;
  quint8 mProto;
  quint8 mAction;
};


class AskQueue : public QObject {
  Q_OBJECT

public:
  AskQueue (QObject *parent = 0);
  ~AskQueue ();

  void append (QLocalSocket *sk, const tIPCQuery &q);
  inline int count () const { return mList.count(); }
  AskQueueItem *get (void); /* can return null */

private:
  static QString getProcName (pid_t pid);

  QList<AskQueueItem *> mList;
};



#endif
