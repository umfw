#ifndef FWRULES_H
#define FWRULES_H

#include "defaults.h"

#include <QString>
#include <QTextStream>


class tFWRuleInfo {
public:
  quint32 ip; /* 0: any */
  quint16 port; /* 0: any */
  quint8 proto; /* 255: any */
  quint8 allow;
  quint8 flags;
  QString descr;
};


class FWRule : public QObject {
  Q_OBJECT

public:
  FWRule (const QString &appName);
  ~FWRule ();

  bool loadFrom (QTextStream &stream);
  bool saveTo (QTextStream &stream) const;

  bool append (const QString &addrdef);

  inline QString getAppName (void) const { return mAppName; }

  /*
   * -1: not me
   *  0: deny
   *  1: allow
   */
  bool getAction (quint32 ip, quint16 port, quint8 proto, quint8 *action, quint8 *flags);

  void addAction (const QString &descr, quint32 ip, quint16 port, quint8 proto, quint8 action);

private:
  void clear ();

  QString mAppName;

  QList<tFWRuleInfo *> mList; /* to save the order */
};


class FWRuleList : public QObject {
  Q_OBJECT

public:
  FWRuleList (const QString &fname = "");
  ~FWRuleList ();

  void clear (void);
  bool loadFrom (const QString &fname);
  bool saveTo (const QString &fname);

  bool getAction (const QString &appName, quint32 ip, quint16 port, quint8 proto, quint8 *allow, quint8 *flags);
  void addAction (const QString &descr, const QString &appName, quint32 ip, quint16 port, quint8 proto, quint8 allow);

private:
  QList<FWRule *> mList; /* to save the order */
};


#endif
