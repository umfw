#include <unistd.h>

#include <QHostAddress>

#include "defaults.h"
#include "uproto.h"

#include "askqueue.h"


AskQueue::AskQueue (QObject *parent) : QObject(parent) {
}


AskQueue::~AskQueue () {
  foreach (AskQueueItem *i, mList) delete i;
  mList.clear();
}


void AskQueue::append (QLocalSocket *sk, const tIPCQuery &q) {
  AskQueueItem *i = new AskQueueItem(0);
  i->mPid = q.pid;
  i->mAppName = getProcName(q.pid);
  QHostAddress ipaddr(q.ip);
  i->mIPStr = ipaddr.toString();
  i->mIP = q.ip;
  i->mPort = q.port;
  i->mProto = q.proto;
  i->mAction = q.action;
  i->mSk = sk;
  mList << i;
}


AskQueueItem *AskQueue::get (void) {
  if (mList.count() < 1) return 0;
  AskQueueItem *res = mList.at(0);
  mList.removeAt(0);
  return res;
}


QString AskQueue::getProcName (pid_t pid) {
  char buf[512], name[4096];
  sprintf(buf, "/proc/%u/exe", pid);
  ssize_t len = readlink(buf, name, sizeof(name));
  if (len < 1) return QString("");
  name[len] = 0;
  return QString(name);
}
