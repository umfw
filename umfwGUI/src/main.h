#ifndef MAIN_H
#define MAIN_H

#include "defaults.h"

#include <QApplication>
#include <QLocalServer>
#include <QLocalSocket>
#include <QSystemTrayIcon>

#include "askqueue.h"
#include "fwrules.h"


class SockReactor : public QObject {
  Q_OBJECT

public:
  SockReactor (QObject *parent = 0);
  ~SockReactor ();

private slots:
  void trayActivated (QSystemTrayIcon::ActivationReason reason);
  void onConnection (void);
  void processQueueStep (void);
  void doIconBlink ();

private:
  void processNextConnection (void);
  void playSound (void);

  void setNormalIcon ();
  void setInvIcon ();

  void blinking (bool doIt);

  QLocalServer *mServer;
  AskQueue mQueue;

  QSystemTrayIcon *mTrayIcon;

  QString mRCPath;
  FWRuleList mRules;
  QString mRulePath;

  int mBlinkPhase;
};



#endif
