#ifndef ASKDIALOG_H
#define ASKDIALOG_H

#include "defaults.h"

#include <QDialog>
#include <QHostInfo>


#include "ui_askdialog.h"
class AskDialog : public QDialog, public Ui_AskDialog {
  Q_OBJECT

public:
  AskDialog (QWidget *parent = 0);
  ~AskDialog ();

  void setInfo (quint8 proto, quint8 action, quint32 pid, const QString &ip, quint16 port,
    const QString &appName, const QString &descr);

  inline QString getDescr (void) const { return edDescr->text(); }
  inline bool getAnyApp (void) const { return cbAnyApp->isChecked(); }
  inline bool getAnyIP (void) const { return cbAnyIP->isChecked(); }
  inline bool getAnyPort (void) const { return cbAnyPort->isChecked(); }
  inline bool getOnlySession (void) const { return cbOnlySession->isChecked(); }
  inline bool getRemember (void) const { return cbRemember->isChecked(); }

private slots:
  void onHostInfo (const QHostInfo &host);

private:
  void updateInfo (void);

  bool mGotHostInfo;
  QString mDescr;
  quint32 mPid;
  QString mAppName;
  QString mIP;
  quint16 mPort;
  QString mHostName;
  quint8 mProto;
  quint8 mAction;

  int mLookupId;
};



#endif
