#include "defaults.h"

#include <QFile>
#include <QHostAddress>
#include <QStringList>

#include "uproto.h"

#include "fwrules.h"


static QString unquoteStr (const QString &str) {
  QString res = str.trimmed();
  if (res.length() >= 2 && res.at(0) == '"' && res.at(res.length()-1) == '"') {
    QString s(res); res.clear();
    for (int f = 1; f < s.length()-1; f++) {
      QChar ch = s.at(f);
      if (ch == '\\') {
        /* special char */
        f++;
        ch = s.at(f);
        if (ch == 't') { res.append('\t'); continue; }
        else if (ch == 'n') { res.append('\n'); continue; }
        else if (ch == 'r') { res.append('\r'); continue; }
      }
      res.append(ch);
    }
  }
  return res;
}


static QString quoteStr (const QString &str) {
  bool needQ = false;
  QString res('"'), s;
  s = str.trimmed();
  for (int f = 0; f < s.length(); f++) {
    QChar ch = s.at(f);
    if (ch == '\t') { res.append("\\t"); needQ = true; }
    else if (ch == '\n') { res.append("\\n"); needQ = true; }
    else if (ch == '\r') { res.append("\\r"); needQ = true; }
    else if (ch == '\\' || ch == '"') {
      res.append("\\");
      res.append(ch);
      needQ = true;
    } else res.append(ch);
  }
  res.append('"');
  if (res.length() != 2) {
    if (!needQ) res = str;
  }
  return res;
}


/*****************************************************************************/
FWRule::FWRule (const QString &appName) : QObject(0), mAppName(appName)
{
}


FWRule::~FWRule () {
  clear();
}


void FWRule::clear () {
  foreach (tFWRuleInfo *i, mList) delete i;
  mList.clear();
}


bool FWRule::saveTo (QTextStream &stream) const {
  stream << "newrule\n";
  stream << " app " << quoteStr(mAppName) << "\n";
  foreach (tFWRuleInfo *i, mList) {
    stream << " rule ";
    if (i->allow) stream << "allow:"; else stream << "deny:";
    switch (i->proto) {
      case IPCQ_PROTO_UDP: stream << "udp:"; break;
      case IPCQ_PROTO_TCP: stream << "tcp:"; break;
      default: stream << "*:"; break;
    }
    if (i->ip == 0) stream << "*:";
    else {
      QHostAddress ipaddr(i->ip);
      stream << ipaddr.toString() << ":";
    }
    if (i->port == 0) stream << "*"; else stream << i->port;
    stream << ":" << quoteStr(i->descr) << "\n";
  }
  stream << "endrule\n";
  return true;
}


/*
newrule
 app *
 rule allow:udp:172.16.20.1:53:DNS query
 rule allow:tcp:172.16.21.162:80:my HTTP server (id)
endrule
*/
bool FWRule::loadFrom (QTextStream &stream) {
  clear();
  for (;;) {
    QString s = stream.readLine();
    if (s.isNull()) return false;
    s = s.trimmed();
    qDebug() << s;
    if (s == "newrule") break;
  }
  qDebug() << "*rule found!";
  QString app;
  for (;;) {
    QString s = stream.readLine();
    if (s.isNull()) return false;
    s = s.trimmed();
    if (s.isEmpty() || s.at(0) == '#') continue;
    if (s == "endrule") break;
    //qDebug() << s;
    if (s.startsWith("app ")) {
      s.remove(0, 4);
      app = unquoteStr(s);
      qDebug() << "app:" << app;
      continue;
    }
    QStringList nv(s.split(' ', QString::SkipEmptyParts));
    if (nv.count() < 2) return false;
    if (nv.at(0) == "rule") {
      //qDebug() << s;
      s.remove(0, 5);
      if (!append(s)) return false;
    } else return false;
  }
  if (mList.count() < 1) return false;
  if (app.isEmpty()) app = "*";
  mAppName = app;
  return true;
}


/*
 allow:udp:172.16.20.1:53:DNS query
*/
static bool splitRuleDef (QStringList *res, const QString &ruledef) {
  //qDebug() << "parsing:" << ruledef;
  for (int n = 0, pos = 0; n < 5; n++) {
    if (n == 4) {
      QString s = ruledef.mid(pos).trimmed();
      s = unquoteStr(s);
      //qDebug() << "n:" << n << "s:" << s;
      res->append(s);
      return true;
    }
    int np = ruledef.indexOf(':', pos);
    if (np == -1) return false;
    QString s = ruledef.mid(pos, np-pos);
    pos = np+1;
    //qDebug() << "n:" << n << "s:" << s;
    s = s.trimmed();
    if (s.isEmpty()) return false;
    res->append(s);
  }
  return false;
}


bool FWRule::append (const QString &ruledef) {
  QStringList nv;
  if (!splitRuleDef(&nv, ruledef)) return false;
  //qDebug() << "nv:" << nv;

  tFWRuleInfo *i = new tFWRuleInfo;
  i->flags = 0;
  /* allow/deny */
  i->allow = (nv.at(0) == "allow")?1:0;
  /* proto */
  QString s;
  QHostAddress ip;
  s = nv.at(1);
  if (s == "tcp") {
    i->proto = IPCQ_PROTO_TCP;
    i->flags |= IPCR_FLAG_TCP;
  } else if (s == "udp") {
    i->proto = IPCQ_PROTO_UDP;
    i->flags |= IPCR_FLAG_UDP;
  } else if (s == "*") {
    i->proto = 255;
    i->flags |= IPCR_FLAG_TCP | IPCR_FLAG_UDP;
  } else goto error;
  /* ip */
  i->flags |= IPCR_FLAG_IP;
  s = nv.at(2);
  if (s == "*") {
    i->ip = 0;
    i->flags |= IPCR_FLAG_ANYIP;
  } else {
    ip.setAddress(s);
    if (ip.isNull() || ip.protocol() != QAbstractSocket::IPv4Protocol) goto error;
    i->ip = ip.toIPv4Address();
  }
  /* port */
  i->flags |= IPCR_FLAG_PORT;
  s = nv.at(3);
  if (s == "*") {
    i->port = 0;
    i->flags |= IPCR_FLAG_ANYPORT;
  } else {
    uint pn; bool ok;
    pn = s.toUInt(&ok, 10);
    if (!ok || pn > 65535) goto error;
    i->port = pn;
  }
  i->descr = nv.at(4);
  mList << i;
  qDebug() <<
    "allow:" << i->allow <<
    "proto:" << i->proto <<
    "flags:" << i->flags <<
    "ip:" << i->ip <<
    "descr:" << i->descr;
  return true;
error:
  delete i;
  return false;
}


void FWRule::addAction (const QString &descr, quint32 ip, quint16 port, quint8 proto, quint8 action) {
  tFWRuleInfo *i = new tFWRuleInfo;

  i->flags = 0;
  i->allow = action;
  i->descr = descr;
  /* proto */
  if (proto == IPCQ_PROTO_TCP) {
    i->proto = IPCQ_PROTO_TCP;
    i->flags |= IPCR_FLAG_TCP;
  } else if (proto == IPCQ_PROTO_UDP) {
    i->proto = IPCQ_PROTO_UDP;
    i->flags |= IPCR_FLAG_UDP;
  } else {
    i->proto = 255;
    i->flags |= IPCR_FLAG_TCP | IPCR_FLAG_UDP;
  }
  /* ip */
  i->flags |= IPCR_FLAG_IP;
  i->ip = ip;
  if (ip == 0) i->flags |= IPCR_FLAG_ANYIP;
  /* port */
  i->flags |= IPCR_FLAG_PORT;
  i->port = port;
  if (port == 0) i->flags |= IPCR_FLAG_ANYPORT;

  mList << i;
}


/*
 * -1: not me
 *  0: deny
 *  1: allow
 */
bool FWRule::getAction (quint32 ip, quint16 port, quint8 proto, quint8 *action, quint8 *flags) {
  foreach (tFWRuleInfo *i, mList) {
    if (i->ip && i->ip != ip) continue;
    if (i->port && i->port != port) continue;
    if (i->proto != 255 && i->proto != proto) continue;
    *action = i->allow;
    *flags = i->flags;
    return true;
  }
  return false;
}


/*****************************************************************************/
FWRuleList::FWRuleList (const QString &fname) : QObject(0) {
  if (!fname.isEmpty()) loadFrom(fname);
}


FWRuleList::~FWRuleList () {
  clear();
}

void FWRuleList::clear (void) {
  foreach (FWRule *r, mList) delete r;
  mList.clear();
}


bool FWRuleList::saveTo (const QString &fname) {
  QFile file(fname);
  if (!file.open(QIODevice::WriteOnly)) return false;

  QTextStream stream;
  stream.setDevice(&file);
  stream.setCodec("UTF-8");
  foreach (FWRule *r, mList) {
    r->saveTo(stream);
  }
  stream.flush();
  stream.setDevice(0);
  file.close();
  return true;
}


bool FWRuleList::loadFrom (const QString &fname) {
  QFile file(fname);
  if (!file.open(QIODevice::ReadOnly)) return false;

  QTextStream stream;
  stream.setDevice(&file);
  stream.setCodec("UTF-8");
  for (;;) {
    FWRule *r = new FWRule("");
    if (!r->loadFrom(stream)) { delete r; break; }
    mList << r;
  }

  file.close();
  return true;
}


bool FWRuleList::getAction (const QString &appName, quint32 ip, quint16 port, quint8 proto, quint8 *allow, quint8 *flags) {
  foreach (FWRule *r, mList) {
    QString an(r->getAppName());
    if (an != "*" && an != appName) continue;
    if (r->getAction(ip, port, proto, allow, flags)) return true;
  }
  return false;
}


void FWRuleList::addAction (const QString &descr, const QString &appName, quint32 ip, quint16 port, quint8 proto, quint8 allow) {
  foreach (FWRule *r, mList) {
    QString an(r->getAppName());
    if (an != appName) continue;
    r->addAction(descr, ip, port, proto, allow);
    return;
  }
  FWRule *r = new FWRule(appName);
  r->addAction(descr, ip, port, proto, allow);
  mList << r;
}
