#include <sys/types.h>
#include <unistd.h>

#include <QFile>
#include <QHostAddress>
#include <QProcess>
#include <QString>
#include <QTimer>
#include <QVector>

#include "uproto.h"

#include "askdialog.h"

#include "main.h"


#define SOUND_CMD  "/usr/bin/aplay"
#define SOUND_ARG  "/home/ketmar/k8prj/usermode_fw/snd/flaps.wav"

#define BLINK_PAUSE  500


SockReactor::SockReactor (QObject *parent) : QObject(parent), mServer(0), mQueue(0), mBlinkPhase(-1) {
  /* remove unix socket */
  QFile fl(UNIX_SOCKET_PATH);
  fl.remove();

  mRCPath = QApplication::applicationDirPath();
  if (mRCPath.isEmpty()) mRCPath = "./";
  else if (mRCPath.at(mRCPath.length()-1) != '/') mRCPath.append('/');

  mRulePath = mRCPath;
  mRulePath.append("rules.rc");
  mRules.loadFrom(mRulePath);

  mServer = new QLocalServer (this);
  connect(mServer, SIGNAL(newConnection()), this, SLOT(onConnection()));
  mServer->listen(UNIX_SOCKET_PATH);
  qDebug() << "server name:" << mServer->fullServerName();
  qDebug() << "listening:" << mServer->isListening();

  if (QSystemTrayIcon::isSystemTrayAvailable()) {
    mTrayIcon = new QSystemTrayIcon(this);
    connect(mTrayIcon, SIGNAL(activated(QSystemTrayIcon::ActivationReason)), this, SLOT(trayActivated(QSystemTrayIcon::ActivationReason)));
    setNormalIcon();
    /*mTrayIcon->setContextMenu(trayMenu);*/
    mTrayIcon->show();
  } else mTrayIcon = 0;
}


SockReactor::~SockReactor () {
}


void SockReactor::setNormalIcon () {
  mTrayIcon->setIcon(QIcon(":/res/blackshield.png"));
  mTrayIcon->setToolTip("<b>UserModeFirewall</b><br>control center");
}


void SockReactor::setInvIcon () {
  mTrayIcon->setIcon(QIcon(":/res/blackshield_inv.png"));
  mTrayIcon->setToolTip("<b>UserModeFirewall WARNING</b><br>control center");
}


void SockReactor::blinking (bool doIt) {
  if (doIt) {
    if (mBlinkPhase < 0) {
      mBlinkPhase = 1;
      setInvIcon();
      QTimer::singleShot(BLINK_PAUSE, this, SLOT(doIconBlink()));
    }
  } else {
    if (mBlinkPhase >= 0) {
      mBlinkPhase = -1;
      setNormalIcon();
    }
  }
}


void SockReactor::doIconBlink () {
  if (mBlinkPhase < 0) return;
  mBlinkPhase = 1-mBlinkPhase;
  if (mBlinkPhase) mTrayIcon->setIcon(QIcon(":/res/blackshield_inv.png"));
  else mTrayIcon->setIcon(QIcon(":/res/blackshield.png"));
  QTimer::singleShot(BLINK_PAUSE, this, SLOT(doIconBlink()));
}


void SockReactor::trayActivated (QSystemTrayIcon::ActivationReason reason) {
  switch (reason) {
    case QSystemTrayIcon::MiddleClick:
      /*QCoreApplication::quit();*/
      QApplication::quit();
      break;
    default: ;
  }
}


void SockReactor::onConnection (void) {
  qDebug() << "connection comes!";
  processNextConnection();
}


void SockReactor::playSound (void) {
/*
  pid_t cp = fork();
  if (cp != 0) return; // fail or parent
  // child
  execl(SOUND_CMD, SOUND_CMD, SOUND_ARG, (char *)NULL);
  // on error
  _exit(1);
*/
}


void SockReactor::processQueueStep (void) {
  AskQueueItem *i = mQueue.get();
  if (!i) { blinking(false); return; }

  tIPCReply pr;
  pr.version = UPROTO_VERSION;

  quint8 allow, flags;
  if (!mRules.getAction(i->mAppName, i->mIP, i->mPort, i->mProto, &allow, &flags)) {
    blinking(true);

    AskDialog dlg;
    dlg.setInfo(i->mProto, i->mAction, i->mPid, i->mIPStr, i->mPort, i->mAppName, "");

    /*
    QProcess *playPrc = new QProcess(0);
    playPrc->start(SOUND_CMD, SOUND_ARG);
    */
    playSound();

    qApp->alert(&dlg, 0);
    switch (dlg.exec()) {
      case QDialog::Accepted: pr.allow = IPCR_ALLOW; break;
      default: pr.allow = IPCR_DENY;
    }
    pr.flags = 0;
    if (dlg.getOnlySession() || dlg.getRemember()) {
      pr.flags = IPCR_FLAG_IP | IPCR_FLAG_PORT;
      if (dlg.getAnyIP()) pr.flags |= IPCR_FLAG_ANYIP;
      if (dlg.getAnyPort()) pr.flags |= IPCR_FLAG_ANYPORT;
      if (i->mProto == IPCQ_PROTO_TCP) pr.flags |= IPCR_FLAG_TCP; else pr.flags |= IPCR_FLAG_UDP;
    }
    if (!dlg.getOnlySession() && dlg.getRemember()) {
      /* create new rule */
      QString appn(i->mAppName);
      if (dlg.getAnyApp()) appn = "*";
      mRules.addAction(dlg.getDescr(), appn, dlg.getAnyIP()?0:i->mIP, dlg.getAnyPort()?0:i->mPort, i->mProto, pr.allow);
      mRules.saveTo(mRulePath);
      qDebug() << "rule added";
    } else if (dlg.getOnlySession()) {
      pr.flags |= IPCR_FLAG_SESSION;
    }
    //qDebug() << "allow:" << pr.allow;
  } else {
    pr.allow = allow;
    pr.flags = flags;
  }

  i->mSk->write((char *)&pr, sizeof(pr));
  i->mSk->waitForBytesWritten(1500);

  //qDebug() << "request complete";
  delete i;

  if (mQueue.count() > 0) QTimer::singleShot(1, this, SLOT(processQueueStep()));
  else blinking(false);
}


void SockReactor::processNextConnection (void) {
  QLocalSocket *sk = mServer->nextPendingConnection();
  do {
    tIPCQuery pq;

    //qDebug() << "waiting...";
    if (!sk->waitForReadyRead(1500)) {
      //qDebug() << "waiting failed";
      break;
    }

    qDebug() << sizeof(pq);
    if (sk->read((char *)&pq, sizeof(pq)) != sizeof(pq)) {
      //qDebug() << "can't read data from local socket";
      break;
    }
    if (pq.version != UPROTO_VERSION) {
      //qDebug() << "invalid protocol version";
      break;
    }

    mQueue.append(sk, pq);
    if (mQueue.count() == 1) QTimer::singleShot(1, this, SLOT(processQueueStep()));
    return;
  } while (0);

  /* error */
  //qDebug() << "processNextConnection(): some error occured";
  delete sk;
  return;
}


int main (int argc, char *argv[]) {
  QApplication app(argc, argv);

  SockReactor rc;

/*
  AskDialog *dlg = new AskDialog();
  dlg->setInfo(23801, "127.0.0.1");
  dlg->show();
*/

  return app.exec();
}
