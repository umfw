TEMPLATE = app
TARGET =
QT += core gui network
CONFIG += qt warn_on
#CONFIG += debug_and_release

#QMAKE_CFLAGS_RELEASE ~= s/\-O./-Os
#QMAKE_CXXFLAGS_RELEASE ~= s/\-O./-Os
#QMAKE_LFLAGS_RELEASE += -s

DESTDIR = .
OBJECTS_DIR = _build/obj
UI_DIR = _build/uic
MOC_DIR = _build/moc
RCC_DIR = _build/rcc

DEPENDPATH += src ../common
INCLUDEPATH += src ../common

FORMS += \
  ui/askdialog.ui

HEADERS += \
  src/defaults.h \
  src/askdialog.h \
  src/askqueue.h \
  src/fwrules.h \
  src/main.h

SOURCES += \
  src/askdialog.cpp \
  src/askqueue.cpp \
  src/fwrules.cpp \
  src/main.cpp

RESOURCES += \
  umfwGUI.qrc
