#ifndef COMMON_N
#define COMMON_N

#include "config.h"


#define LOGMSG_NONE   -1
#define LOGMSG_ERR     0
#define LOGMSG_WARN    1
#define LOGMSG_NOTICE  2
#define LOGMSG_DEBUG   3


#ifndef OPT_RULEPARSER_TEST
static void setLogOptions (int level, const char *filename, int timestamp);
#endif
static void logMsg (int level, const char *fmt, ...) __attribute__((format(printf, 2, 3)));

/*
 * returns static buffer
 */
static char *getProcName (void);


#endif
