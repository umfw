/* Understanding is not required. Only obedience. */
/* atomic integer operations */
/* tAtomicInt guaranteed to be 32 bytes */
/*#include <assert.h>*/
#include <sys/types.h>
#include <unistd.h>

#include "atomic.h"


tAtomicInt atomicCompareExchange (tAtomicInt volatile *var, const tAtomicInt checkVal, const tAtomicInt newVal) {
  // normal OS
  return __sync_val_compare_and_swap(var, checkVal, newVal);
}


tAtomicInt atomicGet (tAtomicInt volatile *var) {
  return atomicCompareExchange(var, 0, 0);
}


/* return old value */
tAtomicInt atomicPut (tAtomicInt volatile *var, const tAtomicInt newVal) {
  tAtomicInt res = atomicCompareExchange(var, newVal, newVal);
  tAtomicInt v = res;
  while (v != newVal) v = atomicCompareExchange(var, v, newVal);
  return res;
}


/* WARNING: these functions doesn't check if lock was properly initialized! */
void atomicInitLock (tAtomicLock volatile *lock) {
  atomicPut(&lock->lock, 0);
  atomicPut(&lock->pid, 0);
}


void atomicDeinitLock (tAtomicLock volatile *lock) {
  atomicPut(&lock->lock, 0);
  atomicPut(&lock->pid, 0);
}


void atomicWaitAndLock (tAtomicLock volatile *lock) {
  tAtomicInt mypid = getpid();
  for (;;) {
    // check and wait
    tAtomicInt pid = atomicCompareExchange(&lock->pid, 0, mypid);
    if (pid == mypid) {
      // we already owns the lock
      lock->lock++;
      return;
    } else if (!pid) {
      // we just aquired the lock
      lock->lock = 1;
      return;
    }
    usleep(1000); // 1ms; 1000000 -- 1 sec
  }
}


void atomicUnlock (tAtomicLock volatile *lock) {
  if (atomicGet(&lock->pid) != getpid()) return;
  tAtomicInt v = atomicGet(&lock->lock);
  /*assert(v > 0);*/
  atomicPut(&lock->lock, v--);
  if (v < 1) atomicPut(&lock->pid, 0);
}
