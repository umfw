#ifndef COMMON_C
#define COMMON_C

#include "config.h"

#include <stdio.h>
#include <netdb.h>
#include <stdarg.h>
#include <errno.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <unistd.h>
#include <arpa/inet.h>
#include <netinet/in.h>

#include "common.h"


#ifndef OPT_RULEPARSER_TEST
/* globals */
static int loglevel = LOGMSG_ERR; /* the default logging level is to only log error messages */
static char logfilename[4096]; /* name of file to which log messages should be redirected */
static FILE *logfile = NULL; /* file to which messages should be logged */
static int logstamp = 0; /* timestamp (and pid stamp) messages */


static void setLogLevel (int level) {
  loglevel = level;
}


/* Set logging options, the options are as follows:             */
/*  level - This sets the logging threshold, messages with      */
/*          a higher level (i.e lower importance) will not be   */
/*          output. For example, if the threshold is set to     */
/*          MSGWARN a call to log a message of level MSGDEBUG   */
/*          would be ignored. This can be set to -1 to disable  */
/*          messages entirely                                   */
/*  filename - This is a filename to which the messages should  */
/*             be logged instead of to standard error           */
/*  timestamp - This indicates that messages should be prefixed */
/*              with timestamps (and the process id)            */
static void setLogOptions (int level, const char *filename, int timestamp) {
  loglevel = level;
  if (loglevel < LOGMSG_ERR) loglevel = LOGMSG_NONE;
  if (filename) {
    strncpy(logfilename, filename, sizeof(logfilename));
    logfilename[sizeof(logfilename)-1] = '\0';
  }
  logstamp = timestamp;
}


static void logMsg (int level, const char *fmt, ...) {
  va_list ap;
  int saveerr;
  extern char *progname;
  char timestring[20];
  time_t timestamp;

  if (loglevel == LOGMSG_NONE || level > loglevel) return;

  saveerr = errno;
  atomicWaitAndLock(&logLock);

  if (!logfile) {
    if (logfilename[0]) {
      logfile = fopen(logfilename, "a");
      if (logfile == NULL) {
        logfile = stderr;
        logMsg(LOGMSG_ERR, "could not open log file, %s, %s\n", logfilename, strerror(errno));
      }
    } else logfile = stderr;
  }

  if (logstamp) {
    timestamp = time(NULL);
    strftime(timestring, sizeof(timestring), "%H:%M:%S", localtime(&timestamp));
    fprintf(logfile, "%s ", timestring);
  }

  fputs(progname, logfile);
  if (logstamp) {
    fprintf(logfile, "(%d)[%s]", getpid(), getProcName());
  }

  fputs(": ", logfile);
  va_start(ap, fmt);
  vfprintf(logfile, fmt, ap);
  va_end(ap);
  fflush(logfile);

  atomicUnlock(&logLock);
  errno = saveerr;
}

#else

static void logMsg (int level, const char *fmt, ...) {
  va_list ap;
  int saveerr;

  saveerr = errno;

  fprintf(stderr, "[%s] ", getProcName());
  va_start(ap, fmt);
  vfprintf(stderr, fmt, ap);
  va_end(ap);
  fflush(stderr);

  errno = saveerr;
}

#endif


static char *getProcName (void) {
  static char name[4096] = {0};
  char buf[512];

  if (name[0]) return name;
  pid_t pid = getpid();
  sprintf(buf, "/proc/%u/exe", pid);
  ssize_t len = readlink(buf, name, sizeof(name));
  if (len < 1) strcpy(name, "<unknown>"); else name[len] = 0;
  return name;
}


#endif
