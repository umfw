/*****************************************************************************/
/* this should be hashed, but i don't care for now */
typedef struct tsSessionRule tSessionRule;
struct tsSessionRule {
  tSessionRule *next;
  uint8_t proto;
  uint32_t ip; /* 0: any */
  uint16_t port; /* 0: any */
  uint8_t allow;
  uint32_t flags;
};

static tSessionRule *sRules = NULL, *sRulesLast = NULL;


#ifdef OPT_USE_RULE_PARSER
static void clearRules (tSessionRule *sRules) {
  while (sRules) {
    tSessionRule *c = sRules;
    sRules = sRules->next;
    free(c);
  }
}


static void clearPermanentRules (void) {
  tSessionRule *p = NULL, *c = sRules;
  while (c) {
    tSessionRule *n = c->next;
    if (!(c->flags & IPCR_FLAG_SESSION)) {
      /* permanent; kill it! */
      if (p) p->next = n; else sRules = n;
      free(c);
    } else p = c;
    c = n;
  }
  sRulesLast = p;
}


#ifdef OPT_RULEPARSER_TEST
static void dumpRules (void) {
  printf("RULES DUMP:\n");
  tSessionRule *c = sRules;
  while (c) {
    char dip[512];
    inet_ntop(AF_INET, &c->ip, dip, sizeof(dip));
    printf("rule: allow=%u, flags=0x%02x, port=%u, ip=%s\n", c->allow, c->flags, c->port, dip);
    c = c->next;
  }
}
#endif

#endif


/*
 * proto: IPCQ_PROTO_TCP, IPCQ_PROTO_UDP or 255 for both
 * ip and port are in network order!
 */
static tSessionRule *findRule (uint32_t ip, uint16_t port, uint8_t proto) {
  logMsg(LOGMSG_DEBUG, "umfw socket: find rule: %08x %u %u\n", ip, port, proto);
  tSessionRule *res = sRules;
  while (res) {
    logMsg(LOGMSG_DEBUG, "umfw socket: rule: %08x %u %u %u\n", res->ip, res->port, res->proto, res->allow);
    if ((res->proto == 255 || res->proto == proto) &&
        (res->ip == 0 || res->ip == ip) &&
        (res->port == 0 || res->port == port)) {
      logMsg(LOGMSG_DEBUG, "umfw socket: rule found: %08x %u %u %u\n", res->ip, res->port, res->proto, res->allow);
      return res;
    }
    res = res->next;
  }
  return NULL;
}


static void addRuleEx (tSessionRule **sRulesH, tSessionRule **sRulesL, uint32_t ip, uint16_t port, uint8_t allow, uint8_t flags) {
  tSessionRule *res = malloc(sizeof(tSessionRule));
  if (!res) return;
  if (flags&IPCR_FLAG_TCP) {
    if (flags&IPCR_FLAG_UDP) res->proto = 255; else res->proto = IPCQ_PROTO_TCP;
  } else if (flags&IPCR_FLAG_UDP) res->proto = IPCQ_PROTO_UDP; else res->proto = 6;
  if (flags&IPCR_FLAG_ANYIP) res->ip = 0; else res->ip = ip;
  if (flags&IPCR_FLAG_ANYPORT) res->port = 0; else res->port = port;
  res->flags = flags;
  res->allow = allow;
  res->next = NULL;
  if (*sRulesL) (*sRulesL)->next = res;
  if (!sRulesH[0]) *sRulesH = res;
  *sRulesL = res;
}


static void addRule (uint32_t ip, uint16_t port, uint8_t allow, uint8_t flags) {
  addRuleEx(&sRules, &sRulesLast, ip, port, allow, flags);
}
