/* Understanding is not required. Only obedience. */
/* atomic integer operations */
/* tAtomicInt guaranteed to be 32 bytes */
#ifndef ATOMIC_H
#define ATOMIC_H

#ifdef __cplusplus
extern "C" {
#endif


#include <stdint.h>


typedef int32_t tAtomicInt;


typedef struct {
  //FIXME: i should use pthreads here!
  volatile tAtomicInt lock;
  volatile tAtomicInt pid;
} tAtomicLock;


/*
 * compare var with checkVar; set var to newVal if values are equal
 * return old var value in any case
 */
tAtomicInt atomicCompareExchange (tAtomicInt volatile *var, const tAtomicInt checkVal, const tAtomicInt newVal);

/*
 * get var value
 */
tAtomicInt atomicGet (tAtomicInt volatile *var);

/*
 * set new value, return old
 */
tAtomicInt atomicPut (tAtomicInt volatile *var, const tAtomicInt newVal); /* return old value */


/* WARNING: these functions doesn't check if lock was properly initialized! */
void atomicInitLock (tAtomicLock volatile *lock);
void atomicDeinitLock (tAtomicLock volatile *lock);
void atomicUnlock (tAtomicLock volatile *lock);
void atomicWaitAndLock (tAtomicLock volatile *lock);


#ifdef __cplusplus
}
#endif


#endif
