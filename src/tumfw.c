/*
 * based on TSOCKS - Wrapper library for transparent SOCKS
 * TSOCKS Copyright (C) 2000 Shaun Clowes
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */
#include "config.h"


/* Global configuration variables */
char *progname = "libtumfw"; /* Name used in err msgs */

/* Header Files */
#include <arpa/inet.h>
#include <dlfcn.h>
#include <errno.h>
#include <fcntl.h>
#include <netinet/in.h>
#include <pwd.h>
#include <stdarg.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <strings.h>
#include <sys/poll.h>
#include <sys/time.h>
#include <sys/types.h>
#include <sys/select.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <sys/un.h>
#include <unistd.h>

#include "common.h"
#include "tumfw.h"
#include "uproto.h"

#include "atomic.c"


static volatile tAtomicLock ruleLock;
static volatile tAtomicLock logLock;
static volatile tAtomicInt initDone = 0;


#include "common.c"


/* global declarations */
static int (*realconnect) (CONNECT_SIGNATURE) = NULL;
static int (*realbind) (BIND_SIGNATURE) = NULL;
static ssize_t (*realsendto) (SENDTO_SIGNATURE) = NULL;


/* exported function prototypes */
//void _init (void);
int connect (CONNECT_SIGNATURE);
int bind (BIND_SIGNATURE);
ssize_t sendto (SENDTO_SIGNATURE);


static int suid = 0;


/*
static void loadSymbols (void) {
}
*/


/*****************************************************************************/
#include "rulecache.c"

#ifdef OPT_USE_RULE_PARSER
#include "ruleparser.c"
#endif


/*****************************************************************************/
static void getEnvironment (void) {
  int loglevel = LOGMSG_ERR;
  char *logfile = NULL;
  int en = errno;
  char *env;
  //
  if (!atomicCompareExchange(&initDone, 0, 1)) {
    if ((env = getenv("TUMFW_DEBUG"))) loglevel = atoi(env);
    if (((env = getenv("TUMFW_DEBUG_FILE"))) && !suid) logfile = env;
    setLogOptions(loglevel, logfile, 1);
#ifdef OPT_USE_RULE_PARSER
    atomicWaitAndLock(&ruleLock);
    initRuleParser();
    atomicUnlock(&ruleLock);
#endif
  }
  errno = en;
  return;
}


static void updateConfig (void) {
#ifdef OPT_USE_RULE_PARSER
  atomicWaitAndLock(&ruleLock);
  updateRuleParser();
  atomicUnlock(&ruleLock);
#endif
  return;
}


/*
 * 0: all ok
 * 1: invalid domain
 * 2: invalid type
 */
static int checkFDandAddr (const char *opname, int __fd, const struct sockaddr_in *connaddr, uint8_t *type) {
  int sock_type = -1;
  int sock_type_len = sizeof(sock_type);

/*
  getsockopt(__fd, SOL_SOCKET, SO_TYPE, (void *)&sock_type, (void *)&sock_type_len);
  logMsg(LOGMSG_DEBUG, "%s: fd: %i; fam: %i; tp: %i\n", opname, __fd, connaddr->sin_family, sock_type);
*/

  /* if this isn't an INET socket we can't handle it, just call the real op */
  if (connaddr->sin_family != AF_INET) {
    logMsg(LOGMSG_DEBUG, "%s: isn't an AF_INET, ignoring\n", opname);
    return 1;
  }

  /* get the type of the socket */
  getsockopt(__fd, SOL_SOCKET, SO_TYPE, (void *)&sock_type, (void *)&sock_type_len);

  if (sock_type != SOCK_STREAM && sock_type != SOCK_DGRAM) {
    logMsg(LOGMSG_DEBUG, "%s: isn't a TCP/UDP, ignoring\n", opname);
    return 2;
  }

  *type = (sock_type == SOCK_STREAM)?IPCQ_PROTO_TCP:IPCQ_PROTO_UDP;

  updateConfig();

  return 0;
}


/* 0: can't */
static int askAllowConnect (const struct in_addr *in, uint16_t port, uint8_t proto, uint8_t action) {
  int oerrno = errno;
  uint32_t *ip = (uint32_t *)in;

  //if (!realbind || !realconnect || !realsendto) return 1; /* no binds -- no checks */
  if (realconnect == NULL) return 1; /* no binds -- no checks */

  if (((*ip>>24)&0xff) == 127) return 1; /* localhost always allowed */

  atomicWaitAndLock(&ruleLock);
  tSessionRule *rule = findRule(*ip, port, proto);
  atomicUnlock(&ruleLock);
  if (rule) return rule->allow;

  int ufd = socket(AF_UNIX, SOCK_STREAM, 0);
  if (ufd == -1) {
    logMsg(LOGMSG_DEBUG, "can't create umfw socket\n");
    errno = oerrno;
    return 1; /* alas, no fd -- no checks */
  }

  struct sockaddr_un srv;
  srv.sun_family = AF_UNIX;
  strcpy(srv.sun_path, UNIX_SOCKET_PATH);
/*
  if (realbind(ufd, (struct sockaddr *)&srv, sizeof(struct sockaddr_un))) {
    logMsg(LOGMSG_DEBUG, "can't bind umfw socket\n");
    goto error;
  }
*/
  if (realconnect(ufd, (struct sockaddr *)&srv, sizeof(struct sockaddr_un))) {
    logMsg(LOGMSG_DEBUG, "can't connect to umfw socket\n");
    goto error;
  }

  tIPCQuery sbuf;
  sbuf.version = UPROTO_VERSION;
  sbuf.pid = getpid();
  sbuf.ip = htonl(*ip);
  sbuf.port = htons(port);
  sbuf.proto = proto;
  sbuf.action = action;
  if (send(ufd, &sbuf, sizeof(sbuf), MSG_NOSIGNAL) != sizeof(sbuf)) {
    logMsg(LOGMSG_DEBUG, "can't send to umfw socket\n");
    goto error;
  }

  tIPCReply rbuf;

  do {
    int r;
    if ((r = recv(ufd, &rbuf, sizeof(rbuf), MSG_WAITALL)) != sizeof(rbuf)) {
      logMsg(LOGMSG_DEBUG, "can't read from umfw socket\n");
      if (errno == EAGAIN || errno == EINTR) {
        logMsg(LOGMSG_DEBUG, "umfw socket: restarting\n");
      }
      goto error;
    } else break;
  } while(1);

  if (rbuf.version != UPROTO_VERSION) {
    logMsg(LOGMSG_DEBUG, "umfw socket: invalid version read\n");
    goto error;
  }

  close(ufd);

  if (rbuf.flags) {
    /* new session/permanent rule */
    atomicWaitAndLock(&ruleLock);
    addRule(*ip, port, rbuf.allow, rbuf.flags);
    atomicUnlock(&ruleLock);
    logMsg(LOGMSG_DEBUG, "umfw socket: new session rule: %08x %u %u %02x\n", *ip, port, rbuf.allow, rbuf.flags);
  }

  errno = oerrno;
  return rbuf.allow == IPCR_ALLOW;

error:
  /* alas, server error -- no checks */
  close(ufd);
  errno = oerrno;
  return 1;
}


/*****************************************************************************/
/* ALAS, dlsym interception segfaults */
/*
extern void *__libc_dlsym (void *handle, const char *name);


void *dlsym (void *handle, const char *name) {
  void *res;
  //
  logMsg(LOGMSG_DEBUG, "dlsym(h=%p, name='%s') (%p)\n", handle, name, __libc_dlsym);
  res = __libc_dlsym(handle, name);
  logMsg(LOGMSG_DEBUG, "dlsym(h=%p, name='%s')=%p\n", handle, name, res);
  return res;
}
*/


static void *getsym (const char *name) {
  //logMsg(LOGMSG_DEBUG, "000[%s] %p %p\n", name, __libc_dlsym, dlsym);
  //void *res = __libc_dlsym(RTLD_NEXT, name);
  void *res = dlsym(RTLD_NEXT, name);
  //logMsg(LOGMSG_DEBUG, "001[%s]: %p\n", name, res);
  if (res) {
    logMsg(LOGMSG_DEBUG, "got symbol '%s'\n", name);
  } else {
    logMsg(LOGMSG_ERR, "ERROR: CAN'T got symbol '%s'\n", name);
  }
  return res;
}


/*****************************************************************************/
#define CHECK_SYMBOL(name) do {\
  getEnvironment(); \
  if (real##name == NULL) { \
    if ((real##name = getsym(#name)) == NULL) { \
      logMsg(LOGMSG_ERR, "unresolved symbol: '%s'\n", #name); \
      return -1; \
    } else { \
      logMsg(LOGMSG_DEBUG, "resolved symbol: '%s'\n", #name); \
    } \
  } \
} while (0)


#define IP_BYTES(n) \
  (uint32_t)(n) & 0xff, \
  (((uint32_t)(n))>>8) & 0xff, \
  (((uint32_t)(n))>>16) & 0xff, \
  (((uint32_t)(n))>>24) & 0xff

static void logAction (const char *action, const struct in_addr *ip, uint16_t port) {

  logMsg(LOGMSG_DEBUG, "*%s: %u.%u.%u.%u:%u\n", action, IP_BYTES(ip->s_addr), (unsigned int)htons(port));
}


int connect (CONNECT_SIGNATURE) {
  struct sockaddr_in *connaddr;
  unsigned char type;

  CHECK_SYMBOL(connect);
  connaddr = (struct sockaddr_in *)__addr;
  if (checkFDandAddr("connect", __fd, connaddr, &type)) goto skipcheck;
  logAction(type==IPCQ_PROTO_TCP?"connect (TCP)":"connect (UDP)", &connaddr->sin_addr, connaddr->sin_port);
  if (!askAllowConnect(&connaddr->sin_addr, connaddr->sin_port, type, IPCQ_ACTION_CONNECT)) {
    errno = ECONNREFUSED;
    return -1;
  }
skipcheck:
  return realconnect(__fd, __addr, __len);
}


int bind (BIND_SIGNATURE) {
  struct sockaddr_in *connaddr;
  unsigned char type;

  CHECK_SYMBOL(bind);
  connaddr = (struct sockaddr_in *)__addr;
  if (checkFDandAddr("bind", __fd, connaddr, &type)) goto skipcheck;
  logAction(type==IPCQ_PROTO_TCP?"bind (TCP)":"bind (UDP)", &connaddr->sin_addr, connaddr->sin_port);
  if (!askAllowConnect(&connaddr->sin_addr, connaddr->sin_port, type, IPCQ_ACTION_BIND)) {
    errno = EADDRNOTAVAIL;
    return -1;
  }
skipcheck:
  return realbind(__fd, __addr, __len);
}


ssize_t sendto (SENDTO_SIGNATURE) {
  const struct sockaddr_in *connaddr;
  unsigned char type;

  CHECK_SYMBOL(sendto);
  if (!__dest_addr) goto skipcheck; /* no dest addr -- nothing to check */
  connaddr = (struct sockaddr_in *)__dest_addr;
  if (checkFDandAddr("sendto", __fd, connaddr, &type)) goto skipcheck;
  logAction("sendto", &connaddr->sin_addr, connaddr->sin_port);
  if (!askAllowConnect(&connaddr->sin_addr, connaddr->sin_port, type, IPCQ_ACTION_SENDTO)) {
    errno = ENOTCONN;
    return -1;
  }
skipcheck:
  return realsendto(__fd, __buf, __len, __flags, __dest_addr, __addrlen);
}


/*
 * We could do all our initialization here, but to be honest
 * most programs that are run won't use our services, so
 * we do our general initialization on first call
 */
/*void _init (void)*/
void __attribute__((constructor)) tumfw_init (void)
{
  /* determine the logging level */
  atomicInitLock(&ruleLock);
  atomicInitLock(&logLock);
  suid = (getuid() != geteuid());
  getEnvironment();
  setLogLevel(LOGMSG_DEBUG);
  /* get 'real' pointers */
  logMsg(LOGMSG_NOTICE, "usermode-firewall library loaded, getting 'real' symbols...\n");
  realconnect = getsym("connect"); // need this for permission checking
  realbind = getsym("bind");
  realsendto = getsym("sendto");
}


void __attribute__((destructor)) tumfw_deinit (void) {
  logMsg(LOGMSG_NOTICE, "usermode-firewall library unloaded\n");
}
