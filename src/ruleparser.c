/*
 */
#include "config.h"

#ifndef _GNU_SOURCE
# define _GNU_SOURCE
#endif

#include <sys/syscall.h>
#include <sys/stat.h>


static struct stat64 ruleFStat;


static int doStat (const char *fname, void *buf) {
  return syscall(SYS_stat64, fname, buf);
}


/*
newrule
 app *
 rule allow:udp:172.16.20.1:53:"DNS query"
 rule allow:*:127.0.0.1:*:"localhost"
 rule deny:*:207.38.11.35:*:"motd.gamespy.com"
endrule
newrule
 app /opt/logjam/bin/logjam
 rule allow:tcp:78.152.169.8:80:"my LiveJournal server"
endrule
*/

static char *loadFile (const char *fname) {
  char *res = NULL;
  int fd = open(fname, O_RDONLY | O_NOATIME);
  if (fd < 0) return NULL;
  off_t size = lseek(fd, 0, SEEK_END);
  if (size <= 0) goto done;
  if (lseek(fd, 0, SEEK_SET) < 0) goto done;
  res = malloc(size+1);
  if (!res) goto done;
  if (read(fd, res, size) != size) { free(res); res = NULL; goto done; }
  res[size] = 0;
done:
  close(fd);
  return res;
}


/*
 * return NULL or ptr
 */
static char *skipBlanks (char **buf) {
  char *res = *buf;
  while (*res && (unsigned char)(*res) <= ' ') res++;
  *buf = res;
  if (!res[0]) res = NULL;
  return res;
}


/*
 * return NULL or ptr
 */
static char *skipLine (char **buf) {
  char *res = *buf;
  while (*res && (unsigned char)(*res) != '\n') res++;
  if (*res) res++;
  *buf = res;
  if (!res[0]) res = NULL;
  return res;
}


/*
 * return ptr to the start of the word or NULL
 * **buf will be changed!
 */
static char *getWord (char **buf) {
  skipBlanks(buf);
  char *res = *buf;
  char *end = res;
  if (*end == '"') {
    char *s = res;
    end++;
    while (*end && *end != '"') {
      if (*end == '\\') {
        char ch = end[1]; end += 2;
        if (!ch) { end--; break; }
        switch (ch) {
          case 't': *s++ = '\t'; break;
          case 'r': *s++ = '\r'; break;
          case 'n': *s++ = '\n'; break;
          default: *s++ = ch; break;
        }
      } else *s++ = *end++;
    }
    *s = 0;
  } else {
    while (*end && (unsigned char)(*end) > ' ') {
      if (*end == ':') break;
      end++;
    }
  }
  if (*end) *end++ = 0;
  *buf = end;
  if (!res[0]) res = NULL;
  return res;
}


/* ok: 0 -- error; -1: ends with dot; 1: normal number; 2: * */
static uint16_t getInt (char **buf, int *ok) {
  *ok = 0;
  char *word = getWord(buf);
  if (!word) return 0;
  if (!strcmp(word, "*")) { *ok = 2; return 0; }
  uint32_t res = 0;
  while (*word) {
    if (*word < '0' || *word > '9') {
      if (*word == '.') *ok = -1;
      return (uint16_t)res;
    }
    res *= 10;
    res += *word-'0';
    if (res & 0x10000) return 0;
    word++;
  }
  *ok = 1;
  return (uint16_t)res;
}


static uint16_t parseInt (char **buf, int *ok) {
  *ok = 0;
  char *word = *buf;
  if (!word[0]) return 0;
  if (!strcmp(word, "*")) { *ok = 2; return 0; }
  uint32_t res = 0;
  while (*word) {
    if (*word < '0' || *word > '9') {
      if (*word == '.') {
        *ok = -1;
        word++;
      }
      *buf = word;
      return (uint16_t)res;
    }
    res *= 10;
    res += *word-'0';
    if (res & 0x10000) return 0;
    word++;
  }
  *buf = word;
  *ok = 1;
  return (uint16_t)res;
}


static uint32_t getIP (char **buf, int *ok) {
  uint32_t res = 0;
  *ok = 0;

  char *word = getWord(buf);
  if (!word) return 0;
  if (!strcmp(word, "*")) { *ok = 1; return 0; }

  int f;
  for (f = 0; f < 4; f++) {
    int kk;
    uint32_t t = parseInt(&word, &kk);
    /*printf("int: ok=%i, int=%u\n", kk, t);*/
    if (f == 0 && kk == 2) { *ok = 1; return 0; }
    if ((f == 3 && kk != 1) || (f != 3 && kk != -1)) return 0;
    res |= t<<(f*8);
  }
  *ok = 1;
  return res;
}


static char *ruleFilePath = NULL;

static void parseRulesFile (void) {
  char *text = NULL, *tpos = NULL;
  if (!ruleFilePath) return;
  text = loadFile(ruleFilePath);
  if (!text) goto error;

  logMsg(LOGMSG_DEBUG, "parsing rule file: '%s'\n", ruleFilePath);

  char *word; tpos = text;
  while ((word = getWord(&tpos))) {
#ifdef OPT_RULEPARSER_TEST
    printf("{%s}\n", word);
#endif
    if (strcmp(word, "newrule")) {
      skipLine(&tpos);
      continue;
    }
    /* scan rule */
    char *app = NULL;
    tSessionRule *sRulesH = NULL, *sRulesL = NULL;
#ifdef OPT_RULEPARSER_TEST
    if (sRulesH) printf("!!!\n");
#endif
    while ((word = getWord(&tpos))) {
      if (!strcmp(word, "endrule")) {
#ifdef OPT_RULEPARSER_TEST
        printf("{%s}\n", word);
#endif
        break;
      }
      if (!strcmp(word, "app")) {
        word = getWord(&tpos);
        if (word && !app) {
          app = strdup(word);
#ifdef OPT_RULEPARSER_TEST
          printf("{app: %s}\n", app);
#endif
        }
      } else if (!strcmp(word, "rule")) {
        /* action, proto, ip, port, descr */
        uint8_t allow, flags = /*IPCR_FLAG_SESSION*/0;
        uint16_t port;
        uint32_t ip;
        do {
          /* action */
          word = getWord(&tpos);
          if (!word) break;
          if (!strcmp(word, "allow")) allow = 1;
          else if (!strcmp(word, "deny")) allow = 0;
          else { skipLine(&tpos); break; }
          /* proto */
          word = getWord(&tpos);
          if (!word) break;
          if (!strcmp(word, "tcp")) flags |= IPCR_FLAG_TCP;
          else if (!strcmp(word, "udp")) flags |= IPCR_FLAG_UDP;
          else if (!strcmp(word, "*")) flags |= IPCR_FLAG_TCP | IPCR_FLAG_UDP;
          else { skipLine(&tpos); break; }
          /* ip */
          int ok;
          ip = getIP(&tpos, &ok);
          /*printf("ok=%i; ip=%u\n", ok, ip);*/
          if (!ok) { skipLine(&tpos); break; }
          flags |= IPCR_FLAG_IP;
          if (!ip) flags |= IPCR_FLAG_ANYIP;
          /* port */
          port = htons(getInt(&tpos, &ok));
          /*printf("ok=%i; port=%u\n", ok, port);*/
          if (!ok) { skipLine(&tpos); break; }
          flags |= IPCR_FLAG_PORT;
          if (!port) flags |= IPCR_FLAG_ANYPORT;
          skipLine(&tpos);
#ifdef OPT_RULEPARSER_TEST
          char dip[512];
          inet_ntop(AF_INET, &ip, dip, sizeof(dip));
          printf("rule: allow=%u, flags=0x%02x, port=%u, ip=%s\n", allow, flags, port, dip);
#endif
          addRuleEx(&sRulesH, &sRulesL, ip, port, allow, flags);
        } while (0);
      } else skipLine(&tpos);
    }
    if (app) {
      if (!strcmp(app, "*") || !strcmp(app, getProcName())) {
        /* this is our rules! */
#ifdef OPT_RULEPARSER_TEST
        tSessionRule *c = sRulesH;
        while (c) {
          char dip[512];
          inet_ntop(AF_INET, &c->ip, dip, sizeof(dip));
          printf("OUR rule: allow=%u, flags=0x%02x, port=%u, ip=%s\n", c->allow, c->flags, c->port, dip);
          c = c->next;
        }
#endif
        clearPermanentRules();
        /* add to list */
        if (sRulesLast) sRulesLast->next = sRulesH; else sRules = sRulesH;
        sRulesLast = sRulesL;
        sRulesH = NULL;
      }
      free(app);
    }
    clearRules(sRulesH);
  }
  goto done;
error:
  free(ruleFilePath);
  ruleFilePath = NULL;
done:
  if (text) free(text);
#ifdef OPT_RULEPARSER_TEST
  dumpRules();
#endif
}


static void initRuleParser (void) {
#ifdef OPT_RULEPARSER_TEST
  ruleFilePath = strdup("./umfwGUI/rules.rc");
#else
  char *env = getenv("TUMFW_RULEFILE");
  if (env) ruleFilePath = strdup(env);
#endif
  parseRulesFile();
  if (!ruleFilePath) return;
  if (doStat(ruleFilePath, &ruleFStat)) {
    logMsg(LOGMSG_ERR, "stat64() failed!\n");
  }
}


static void updateRuleParser (void) {
  if (!ruleFilePath) return;
  struct stat64 st;
  int en = errno;
  int res = doStat(ruleFilePath, &st);
  if (res) {
    logMsg(LOGMSG_ERR, "stat64() failed!\n");
    goto done;
  }
  if (st.st_mtime == ruleFStat.st_mtime) goto done;
  logMsg(LOGMSG_DEBUG, "reloading rules...\n");
  parseRulesFile();
  if (ruleFilePath) {
    doStat(ruleFilePath, &ruleFStat);
  }
done:
  errno = en;
  return;
}
