#ifndef CONFIG_H
#define CONFIG_H


#define CONNECT_SIGNATURE   int __fd, const struct sockaddr * __addr, socklen_t __len
#define BIND_SIGNATURE      int __fd, const struct sockaddr *__addr, socklen_t __len

#define SENDTO_SIGNATURE    int __fd, const void *__buf, size_t __len, int __flags, const struct sockaddr *__dest_addr, socklen_t __addrlen
#define RECVFROM_SIGNATURE  int __fd, void *__buf, size_t __len, int __flags, struct sockaddr *__src_addr, socklen_t *__addrlen


#ifndef _GNU_SOURCE
# define _GNU_SOURCE
#endif


#endif
