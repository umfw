#ifndef OPT_USE_RULE_PARSER
# define OPT_USE_RULE_PARSER
#endif

#define OPT_RULEPARSER_TEST

#include <arpa/inet.h>
#include <errno.h>
#include <fcntl.h>
#include <stdarg.h>
#include <stdint.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>

#include "uproto.h"
#include "common.h"

#include "common.c"
#include "rulecache.c"
#include "ruleparser.c"


int main () {
  initRuleParser();
  return 0;
}
