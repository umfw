#ifndef UPROTO_H
#define UPROTO_H

#ifdef __cplusplus
extern "C" {
#endif

#include <stdint.h>
#include <sys/types.h>
#include <unistd.h>


#define UNIX_SOCKET_PATH  "/tmp/umfw.socket"

#define UPROTO_VERSION  0


#define IPCQ_PROTO_TCP  0
#define IPCQ_PROTO_UDP  1

#define IPCQ_ACTION_CONNECT   0
#define IPCQ_ACTION_BIND      1
#define IPCQ_ACTION_SENDTO    2

typedef struct __attribute__((packed)) {
  uint8_t version;
  pid_t pid; /* process id */
  uint8_t proto; /* IPCQ_PROTO_xxx */
  uint8_t action; /* IPCQ_ACTION_xxx */
  uint32_t ip; /* network order */
  uint16_t port; /* machine order */
} tIPCQuery;


#define IPCR_DENY   0
#define IPCR_ALLOW  1

/* what should be permanent for this process */
#define IPCR_FLAG_IP       0x01
#define IPCR_FLAG_PORT     0x02
#define IPCR_FLAG_TCP      0x04
#define IPCR_FLAG_UDP      0x08
#define IPCR_FLAG_ANYIP    0x10
#define IPCR_FLAG_ANYPORT  0x20

/* non-permanent flag */
#define IPCR_FLAG_SESSION  0x80

typedef struct __attribute__((packed)) {
  uint8_t version;
  uint8_t allow; /* IPCR_DENY/IPCR_ALLOW */
  uint8_t flags; /* set of IPCR_FLAG_xxx */
} tIPCReply;


#ifdef __cplusplus
}
#endif

#endif
